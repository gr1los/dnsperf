CXX = g++
FILES = ./src/DnsResolver/DnsResolver.cpp \
		./src/DbConnector/DbConnector.cpp \
		./src/main.cpp
INC=-I/usr/include/mysql++ -I/usr/include/mysql
LDFLAGS := -lmysqlpp -lmysqlclient -lldns

default: main

main: ./src/main.cpp
	mkdir -p out
	$(CXX) -Wall -std=c++11 $(FILES) $(INC) $(LDFLAGS) -o ./out/dnsperf.out

debug:
	mkdir -p out
	$(CXX) -Wall -std=c++11 -g $(FILES) $(INC) $(LDFLAGS) -o ./out/dnsperf_debug.out

check:
	valgrind -v --leak-check=full --show-leak-kinds=all --track-origins=yes ./out/dnsperf.out 1

clean:
	-rm -f ./out/dnsperf.out
	-rm -f ./out/dnsperf_debug.out
