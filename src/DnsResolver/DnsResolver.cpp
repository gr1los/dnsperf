#include "DnsResolver.h"

using namespace std;

DnsResolver::DnsResolver() { GenRandomString(); }

DnsResolver::~DnsResolver() {}

uint32_t DnsResolver::GetQueryTime(string dname) {
    uint32_t query_time;

    string dname_with_pfx = random_string_ + '.' + dname;

    domain_ = ldns_dname_new_frm_str(dname_with_pfx.c_str());
    s_ = ldns_resolver_new_frm_file(&res_, NULL);
    pkt_ = ldns_resolver_query(res_, domain_, LDNS_RR_TYPE_NS, LDNS_RR_CLASS_IN,
                               LDNS_RD);

    ns_ = ldns_pkt_rr_list_by_type(pkt_, LDNS_RR_TYPE_NS, LDNS_SECTION_ANSWER);
    query_time = ldns_pkt_querytime(pkt_);

    cout << dname_with_pfx << setw(20 - dname_with_pfx.length()) << "\t"
         << query_time << endl;

    // free up vars that point to memory
    ldns_rr_list_deep_free(ns_);
    ldns_resolver_deep_free(res_);
    ldns_rdf_deep_free(domain_);
    ldns_pkt_free(pkt_);

    return query_time;
}

void DnsResolver::GenRandomString(size_t len /* = 5 */) {
    // remove old random string
    random_string_.clear();
    generate_n(back_inserter(random_string_), len,
               [&] { return default_chars_[dist(gen_)]; });
}
