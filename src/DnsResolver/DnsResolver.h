#ifndef DNSRESOLVER_H_
#define DNSRESOLVER_H_

#include <iostream>
#include <string>
#include <cstring>
#include <random>
#include <algorithm>
#include <iomanip>
#include <ldns/ldns.h>

using namespace std;

/**
 * This class is responsible for dns query
 * execution using random prefixes for every
 * dname: <RandomString>.google.com
 */
class DnsResolver {
   public:
    /**
     * Constructs a new DnsResolver.
     */
    DnsResolver();

    /**
     * Deconstructs a DbConnector freeing all
     * space that previous allocated from it.
     */
    virtual ~DnsResolver();

    /**
     * This function initializes and executes a DNS
     * query (NS TYPE) to the specified dname.
     *
     * @param dname the dname's index into the list.
     * @return query time that the dns request received.
     */
    uint32_t GetQueryTime(string dname);

    /**
     * This is a random string generator based on
     * mt19937 (Mersenne Twister) generator algorithm.
     *
     * @param len the length of the generated string.
     */
    void GenRandomString(size_t len = 5);

   private:
    /**
     * a private variable
     * this variable contains resolver's data.
     */
    ldns_resolver *res_;

    /**
     * a private variable
     * contains dname information in rdf format.
     */
    ldns_rdf *domain_;

    /**
     * a private variable
     * this variable keeps in rdf format domain's details.
     */
    ldns_pkt *pkt_;

    /**
     * a private variable
     * holds all DNS Resource Records from pkt_.
     */
    ldns_rr_list *ns_;

    /**
     * a private variable
     * this variable refers to the status of the DNS resolver.
     */
    ldns_status s_;

    /**
     * a private variable
     * variable helper for random string generation.
     */
    string const default_chars_ =
        "abcdefghijklmnaoqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    /**
     * a private variable
     * the generated random string.
     */
    string random_string_;

    /**
     * a private variable
     * mt19937 generator seed from random device.
     */
    mt19937 gen_{random_device()()};

    /**
     * a private variable
     * this variable describes the distribution of the random
     * variables that they need to be generated.
     */
    uniform_int_distribution<size_t> dist{
        0, default_chars_.length() - 1};  // define the range
};

#endif /* DNSRESOLVER_H_ */
