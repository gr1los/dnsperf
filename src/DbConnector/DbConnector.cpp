#include "DbConnector.h"

using namespace std;
using namespace mysqlpp;

DbConnector::DbConnector(vector<string> dnames, string db, string ip,
                         string uname, string pass)
    : dnames_(dnames), db_(db), ip_(ip), uname_(uname), pass_(pass) {
    // some robust code for exception handling
    // before database initialization
    try {
        db_conn_ = Connection("", ip_.c_str(), uname_.c_str(), pass_.c_str());
        db_conn_.create_db(db_.c_str());
    } catch (const BadQuery& er) {  // handle any query errors
        cerr << "Query Error: " << er.what() << endl;
    } catch (const ConnectionFailed& er) {  // handle connection errors
        cerr << "Connection Failed: " << er.what() << endl;
    } catch (const Exception& er) {  // any other error
        cerr << "Generic Error: " << er.what() << endl;
    }
    db_conn_.select_db(db_.c_str());
    Query query = db_conn_.query();
    Query view_query = db_conn_.query();

    // init view query
    view_query << "drop view if exists Metrics;";
    view_query.exec();
    view_query << "create view Metrics as" << endl;

    // we need to iterate two vectors with a dummy for loop
    // the first one includes dnames of this form: google.com
    // and we need to replace every dot(.) with underscore(_)
    // because dot in MySql is the table selection symbol
    for (unsigned int i = 0; i < dnames.size(); ++i) {
        replace(dnames_.at(i).begin(), dnames_.at(i).end(), '.', '_');
        string original_dname = dnames.at(i);
        string edited_dname = dnames_.at(i);

        // construct dnames table
        query << "create table if not exists " << edited_dname
              << "(TIME int unsigned, TIMESTAMP int unsigned);";
        query.exec();

        // construct view query
        if (i) view_query << "union all" << endl;
        view_query
            << "select" << endl
            << "\"" << original_dname << "\" as dname," << endl
            << "AVG(TIME) as AVERAGE," << endl
            << "STDDEV(TIME) as STANDARD_DEVIATION," << endl
            << "COUNT(TIME) as NUMBER_OF_QUERIES," << endl
            << "(select TIMESTAMP from " << edited_dname
            << " limit 1) as FIRST_QUERY_TIMESTAMP," << endl
            << "(select TIMESTAMP from " << edited_dname
            << " order by TIMESTAMP desc limit 1) as LAST_QUERY_TIMESTAMP"
            << endl
            << "from " << edited_dname << endl;
    }
    view_query.exec();
}

DbConnector::~DbConnector() {
    db_conn_.disconnect();  // disconnect from db
    db_conn_.thread_end();  // end the current DbDriver thread
    mysql_library_end();    // end the underlaying mysqlclient lib
}

void DbConnector::InsertQuery(size_t dname_index, uint32_t query_time) {
    Query query = db_conn_.query();
    query << "insert into " << dnames_.at(dname_index) << " values ("
          << query_time << ", UNIX_TIMESTAMP());";
    // cout << query.str() << endl;
    query.exec();
}