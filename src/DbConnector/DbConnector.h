#ifndef DBCONNECTOR_H_
#define DBCONNECTOR_H_

#include <iostream>
#include <algorithm>

// a dirty hack to disable and enable the following diagnostic
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <mysql++.h>
#pragma GCC diagnostic warning "-Wdeprecated-declarations"

using namespace std;
using namespace mysqlpp;

/**
 * This class is responsible for mysql connection
 * and query building and execution.
 */
class DbConnector {
   public:
    /**
     * Constructs a new DbConnector connecting and
     * initializing the MySql database.
     *
     * @param dnames the dname's vector.
     * @param db , the name of the database.
     * @param ip , the ip of the MySql server
     * @param uname , the username of MySql server.
     * @param pass , the password of MySql server
     */
    DbConnector(vector<string> dnames, string db, string ip, string uname,
                string pass);

    /**
     * Deconstructs a DbConnector freeing all
     * space that previous allocated from it.
     */
    virtual ~DbConnector();

    /**
     * This function inserts into the corresponding table
     * the generated query time from a dns response.

     * @param dname_index the dname's index into the list.
     * @param query_time the dns query time.
     */
    void InsertQuery(size_t dname_index, uint32_t query_time);

   private:
    /**
     * a private variable
     * this variable keeps the MySql db connection.
     */
    Connection db_conn_;

    /**
     * a private variable
     * this variable holds the dname
     * replacing each dot with underscore
     * ex: google.com -> google_com
     */
    vector<string> dnames_;

    /**
     * a private variable
     * this variable holds the database name
     */
    string db_;

    /**
     * a private variable
     * this variable holds the ip of MySql server
     */
    string ip_;

    /**
     * a private variable
     * this variable holds the username of MySql server
     */
    string uname_;

    /**
     * a private variable
     * this variable holds the password of MySql server
     */
    string pass_;
};

#endif /* DBCONNECTOR_H_ */
