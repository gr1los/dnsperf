#include <iostream>
#include <chrono>
#include <thread>
#include <atomic>
#include <csignal>

#include "./DnsResolver/DnsResolver.h"
#include "./DbConnector/DbConnector.h"

using namespace std;

volatile atomic<bool> quit(false);  // signal flag

vector<string> dnames = {"google.com", "facebook.com", "youtube.com",
                         "yahoo.com",  "live.com",     "wikipedia.org",
                         "baidu.com",  "blogger.com",  "msn.com",
                         "qq.com",     "uoc.gr"};

void sigHandle(int s) { quit = true; }

int main(int argc, char* argv[]) {
    // init signal handler
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigHandle;
    sigfillset(&sa.sa_mask);
    sigaction(SIGINT, &sa, nullptr);

    // command line arguments checks
    if (argc > 2) {
        cerr << "Wrong arguments!" << endl;
        cerr << "Usage:" << argv[0];
        cerr << " <time interval (sec)>" << endl;
        exit(EXIT_FAILURE);
    }

    size_t time_interval = 3;

    if (argc == 1) {
        cerr << "Using default interval = " << time_interval << endl;
    }

    if (argc == 2) {
        try {
            if (stoi(argv[1], nullptr, 10) < 0) {
                cerr << "Negative value " << argv[1] << endl;
                cerr << "Using default interval = " << time_interval << endl;
            } else {
                time_interval = stoi(argv[1], nullptr, 10);
            }
        } catch (const invalid_argument& ia) {
            cerr << "Invalid argument " << argv[1] << endl;
            cerr << "Using default interval = " << time_interval << endl;
        } catch (const out_of_range& oof) {
            cerr << "Out of range " << argv[1] << endl;
            cerr << "Using default interval = " << time_interval << endl;
        }
    }
    // end of command line checks

    DnsResolver res;
    DbConnector con(dnames, "dnsperf", "localhost", "root", "");

    while (true) {
        size_t index = 0;
        for (auto dname : dnames)
            con.InsertQuery(index++, res.GetQueryTime(dname));

        this_thread::sleep_for(chrono::seconds(time_interval));
        res.GenRandomString();
        cout << endl;

        if (quit) break;
    }

    return EXIT_SUCCESS;
}