# DNSPERF (Dns Perfomance Monitor)

### What does it do?
* Batch DNS dname resolving
* DNS cache avoidance using random prefix on each dname
* Provides metric data for every query that receives using MySQL
* Metrics data includes :
    * Average query times.
    * Standard deviation of queries.
    * Number of queries.
    * First/Last query timestamp (Unix time).

### How to build?
* Install Dependencies (are included in every Debian's based distro package list)
    * g++ >= 4:5.2.1
    * libldns-dev >= 1.6.17-5
    * libmysql++-dev >= 3.2.1
    * make >= 4.0-8.2
* Clone this project
* cd dnsperf/
* make

### How to use?
* Clone this project
* cd dnsperf/
* Build the project
* ./out/dnsperf.out [time_interval (sec)]
* Using your favorite mysql client open dnsperf database:
    * Each table includes the query time and timestamp of each query
    * Inside Views you can find the Metrics data table.

**Note:** time_interval specifies the frequency of DNS queries
and default value is 3

### Makefile rules:
* make debug
* make check

### Known "bugs":
* Valgrind bug for "still reachable" memory when using libstdc++ from gcc 5

https://bugs.kde.org/show_bug.cgi?id=345307
